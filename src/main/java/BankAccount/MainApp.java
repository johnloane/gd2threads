package BankAccount;

public class MainApp {
    public static void main(String[] args) {
        Account niallsAccount = new Account(1000);
        Runnable runner = new AccountThread(niallsAccount);
        System.out.println("Starting all of the threads");

        Thread t1 = new Thread(runner);
        Thread t2 = new Thread(runner);
        Thread t3 = new Thread(runner);

        t1.start();
        t2.start();
        t3.start();

        //Wait for the threads to finish
        try {
            t1.join();
            t2.join();
            t3.join();
        }catch(InterruptedException ie){
            ie.printStackTrace();
        }

        System.out.println("Account balance is: " + niallsAccount.getBalance());
    }
}
