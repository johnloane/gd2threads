package BankAccount;

public class AccountThread implements Runnable {
    Account acc;

    public AccountThread(Account acc) {
        this.acc = acc;
    }

    public void run(){
        //Try to deposit 100, 10 times
        for(int i=0; i < 10; ++i){
            acc.deposit(100);
        }
    }
}
