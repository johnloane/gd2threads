package BankAccount;

public class Account {
    private double balance;
    private static Object sharedLock = new Object();

    public Account(double startBalance) {
        this.balance = startBalance;
    }

    public Account() {
        this.balance = 0.0;
    }

    public double getBalance() {
        return balance;
    }

    public void deposit(double amount){
        double temp;

        synchronized (sharedLock) {

            temp = this.balance;

            try {
                Thread.sleep(500);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
            if (amount >= 0) {
                temp = temp + amount;
                balance = temp;
            }
        }
    }

    public void withdraw(double amount){
        try{
            Thread.sleep(500);
        }catch(InterruptedException ie){
            ie.printStackTrace();
        }
        if(amount >= 0 && amount <= this.balance) {
            balance -= amount;
        }
    }
}
