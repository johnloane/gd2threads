package TerminateThread;

/* A class to show why synchronized methods are needed*/

public class RepeatedMessage implements Runnable {

    private String message;
    private int pauseTime;
    /* Declared volatile so that every thread sees the change immediately*/
    private volatile boolean stopFlag;
    private static Object sharedLock = new Object();

    /* Construct a repeating message
    @param inputMessage the message to be repeated
    @param inputPauseTime the time in ms between each character being output
     */

    public RepeatedMessage(String message, int pauseTime) {
        this.message = message;
        this.pauseTime = pauseTime;
    }

    /* Display a repeating message
    @param rm the message to be repeated
    @exception InterruptedException if the thread does
    not sleep for the full time specified
     */
    public static void displayMessage(RepeatedMessage rm) throws InterruptedException{
            synchronized (sharedLock) {
                for (int i = 0; i < rm.message.length(); ++i) {
                    System.out.print(rm.message.charAt(i));
                    Thread.currentThread().sleep(50);
                }
                System.out.println();
            }

    }

    //The workings of the thread
    public void run(){
        stopFlag = false;

        try{
            while(!stopFlag) {
                displayMessage(this);
                Thread.currentThread().sleep(pauseTime);
            }
        }catch(InterruptedException e){
            e.printStackTrace();
        }
    }

    //A method to turn the stopFlag on
    public void finish(){
        stopFlag = true;
        return;
    }


}
