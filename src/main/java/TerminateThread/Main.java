package TerminateThread;

public class Main {

    public static void main(String[] args) {
        try{
            RepeatedMessage m1 = new RepeatedMessage("HelloThere", 500);
            Thread m1T = new Thread(m1);
            m1T.start();

            RepeatedMessage m2 = new RepeatedMessage("Czesc", 111);
            Thread m2T = new Thread(m2);
            m2T.start();

            RepeatedMessage m3 = new RepeatedMessage("Privat", 333);
            Thread m3T = new Thread(m3);
            m3T.start();

            //I don't want the threads to run forever
            //Pause to let the threads run
            //Then stop them. Pause time 5 seconds
            Thread.currentThread().sleep(5000);
            m1.finish();
            m2.finish();
            m3.finish();
        }catch (InterruptedException ie){
            ie.printStackTrace();
        }finally{
            //Flush the output buffer
            System.out.println();
        }

    }
}
