package SharedResource;

public class MainApp {
    public static void main(String[] args) {
        Resource sharedResource = new Resource();
        //control or cmd d copies a line

        Thread t1 = new Thread(new ResourceUserThread("t1", sharedResource));
        Thread t2 = new Thread(new ResourceUserThread("t2", sharedResource));
        Thread t3 = new Thread(new ResourceUserThread("t3", sharedResource));
        Thread t4 = new Thread(new ResourceUserThread("t4", sharedResource));

        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}
