package SharedResource;

public class Resource {
    private boolean connectionFree;

    public Resource() {
        this.connectionFree = true;
    }

    public synchronized void getConnection(String name){
        System.out.println(name + " entered getConnection()");
        while(connectionFree == false){
            try{
                System.out.println("Waiting in getConnection()");
                wait();
                System.out.println(name + " notified of a change");
            }catch(InterruptedException iex){
                System.err.print(iex);
            }
        }
        connectionFree = false;
        notifyAll();
    }

    public synchronized void freeConnection(String name){
        System.out.println(name + " entered freeConnection()");
        while(connectionFree == true){
            try{
                wait();
            }catch(InterruptedException iex){
                System.err.println(iex);
            }
        }
        connectionFree = true;
        notifyAll();
    }
}
