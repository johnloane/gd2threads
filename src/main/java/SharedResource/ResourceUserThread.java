package SharedResource;

public class ResourceUserThread implements Runnable {
    private String name;
    private Resource resource;

    public ResourceUserThread(String name, Resource resource) {
        this.name = name;
        this.resource = resource;
    }

    public void run(){
        resource.getConnection(name);

        try{
            Thread.sleep(2000);
        }catch(InterruptedException iex){
            System.err.println(iex);
        }

        resource.freeConnection(name);
    }
}
