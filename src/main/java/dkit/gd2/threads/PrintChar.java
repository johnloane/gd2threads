package dkit.gd2.threads;

/* The thread class prints a specified character a specified number of times
 */

public class PrintChar extends Thread {
    private char charToPrint;
    private int times;

    public PrintChar(char c, int t){
        charToPrint = c;
        times = t;
    }

    //Override the run method to tell the system
    //what the thread will do
    @Override
    public void run(){
        for(int i=0; i < times; i++){
            System.out.println(charToPrint);
        }
    }
}
