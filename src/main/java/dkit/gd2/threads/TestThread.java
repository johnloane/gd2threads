package dkit.gd2.threads;

public class TestThread {
    public static void main(String[] args) {
        //Create the threads
        PrintChar printA = new PrintChar('a', 100);
        PrintChar printB = new PrintChar('b', 100);
        PrintNumber print100 = new PrintNumber(100);

        //Start the threads
        print100.start();
        printA.start();
        printB.start();

    }
}
