package runnableThreads;

public class SumLowHigh implements Runnable {
    private int low;
    private int high;

    public SumLowHigh(int low, int high) {
        this.low = low;
        this.high = high;
    }

    public void run(){
        int total = 0;

        for(int i=low; i <= high; i++){
            total += i;
        }
        System.out.println("Sum from " + low + " to " + high + " is " + total);
    }
}
