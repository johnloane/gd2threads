package runnableThreads;

/* This class prints from 1 to n where n is the number given when constructing the class*/

public class PrintNumber implements Runnable{
    private int lastNumber;

    public PrintNumber(int n){
        lastNumber = n;
    }

    public void run(){
        for(int i=1; i <= lastNumber; i++){
            System.out.println(" " + i);
        }
    }
}

