package runnableThreads;

public class TestRunnable {
    public static void main(String[] args) {
        //Create the threads
//        PrintChar printARunnable = new PrintChar('a', 100);
//        PrintChar printBRunnable = new PrintChar('b', 100);
//        PrintNumber print100Runnable = new PrintNumber(100);
//
//        /* When implementing runnable, need to create a thread passing in the runnable object into the thread constructor. Only then can we start the thread
//         */
//
//        Thread printA = new Thread(printARunnable);
//        Thread printB = new Thread(printBRunnable);
//        Thread print100 = new Thread(print100Runnable);
//
//        //Start the threads
//        print100.start();
//        printA.start();
//        printB.start();

        SumLowHigh sum125 = new SumLowHigh(1, 25);
        SumLowHigh sum2650 = new SumLowHigh(26, 50);
        SumLowHigh sum5175 = new SumLowHigh(51, 75);
        SumLowHigh sum76100 = new SumLowHigh(76, 100);

        Thread sum125T = new Thread(sum125);
        Thread sum2650T = new Thread(sum2650);
        Thread sum5175T = new Thread(sum5175);
        Thread sum76100T = new Thread(sum76100);

        sum125T.start();
        sum2650T.start();
        sum5175T.start();
        sum76100T.start();





    }
}
