package Scheduling;
/* Class to demonstrate using the Timer utility to schedule
tasks on a background thread
 */
import java.util.TimerTask;

public class ScheduledTask extends TimerTask {
    String message;

    public ScheduledTask(String message) {
        this.message = message;
    }

    //The task is to print the message to the console
    public void run(){
        System.out.println(message);
    }

}
