package Scheduling;

import java.util.Timer;

public class RealisticConversation {

    public static void main(String[] args) {
        ScheduledTask hi1 = new ScheduledTask("Hi");
        ScheduledTask hello2 = new ScheduledTask("Hello");
        ScheduledTask howAreYou = new ScheduledTask("How are you?");
        ScheduledTask notGood = new ScheduledTask("Not good");
        ScheduledTask okBye = new ScheduledTask("Ok, bye");

        //run a timer
        Timer timer = new Timer(true);
        timer.schedule(hi1, 1000);
        timer.schedule(hello2, 2000);
        timer.schedule(howAreYou, 3000);
        timer.schedule(notGood, 3500);
        timer.schedule(okBye, 10000);

        try{
            Thread.currentThread().sleep(11000);
        }catch(InterruptedException iex){
            iex.printStackTrace();
        }

    }
}
