package ThreadGroups;

public class PiggyBank {
    private int balance;

    public PiggyBank(int balance) {
        this.balance = balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getBalance() {
        return balance;
    }
}
