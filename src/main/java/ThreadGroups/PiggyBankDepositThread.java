package ThreadGroups;

public class PiggyBankDepositThread implements Runnable {
    private PiggyBank bank;

    public PiggyBankDepositThread(PiggyBank bank) {
        this.bank = bank;
    }

    public void run(){
        addAPenny(bank);
    }

    //Add a penny to the piggy bank
    private static synchronized void addAPenny(PiggyBank bank){
        int newBalance = bank.getBalance() + 1;

        try{
            Thread.sleep(5);
        }catch(InterruptedException iex){
            iex.printStackTrace();
        }
        bank.setBalance(newBalance);
    }
}
