package Pipes;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class MainPipeApp {
    public static void main(String[] args) {
        try{
            //Create a stream for writing
            PipedOutputStream pout = new PipedOutputStream();

            //Create a pipe for reading, need to connect
            //to the writing pipe
            PipedInputStream pin = new PipedInputStream(pout);

            //Create a new pipe demo thread to write to our pipe
            PipeDemo pipeDemo = new PipeDemo(pout);
            Thread pipeDemoT = new Thread(pipeDemo);

            pipeDemoT.start();

            //Read the data from the thread
            int input = pin.read();

            System.out.println(Thread.currentThread().getName());
            //Terminate when the end of the stream is reached
            while(input != -1){
                //Print message
                System.out.print((char) input);
                //Read next byte
                input = pin.read();
            }
        }catch(IOException iox){
            iox.printStackTrace();
        }
    }
}
