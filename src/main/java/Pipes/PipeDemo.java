package Pipes;

import java.io.PipedOutputStream;
import java.io.PrintStream;

public class PipeDemo implements Runnable {
    PipedOutputStream output;

    public PipeDemo(PipedOutputStream output) {
        this.output = output;
    }

    public void run(){
        try {
            //Create a printStream for convenient writing
            PrintStream p = new PrintStream(output);
            //Print a message
            p.println("Hello from the thread, using pipes!");
            //Close the stream
            p.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
