package ThreadCommunication;

public class BankingMainApp {
    /* The test method for the class
        @param args[0] Time in seconds that we want to the application to run for
     */

    public static void main(String[] args) {
        BankAccount bankAccount = new BankAccount();

        //Create the spender thread
        Spender spenderThread = new Spender(bankAccount);

        //Create the saver thread
        Saver rSaver = new Saver(bankAccount);
        Thread saverThread = new Thread(rSaver);

        spenderThread.start();
        saverThread.start();

        int time;

        if(args.length == 0){
            time = 10000;
        }else{
            time = Integer.parseInt(args[0]) * 1000;
        }
        try{
            Thread.currentThread().sleep(time);
        }catch(InterruptedException iex){
            System.err.println(iex);
        }
        bankAccount.close();
    }
}
