package ThreadCommunication;

public class Saver implements Runnable {
    private BankAccount account;

    /* Class constructor
        @param account The bank account where the saver saves
     */

    public Saver(BankAccount account) {
        this.account = account;
    }

    /* The method the saver uses to save money*/

    public void run(){
        while(account.isOpen()){
            try{
                if(account.deposit(100)){
                    System.out.println("100 successfully deposited");
                }
                Thread.currentThread().sleep(1000);
            }catch(InterruptedException ie){
                System.err.println(ie);
            }
        }
    }
}
