package ThreadCommunication;
/* Class to demonstrate wait() and notify() methods
   To show how threads communicate
 */

public class BankAccount {
    private double balance = 0;
    private boolean isOpen = true;

    /* This method withdraws from the account. If the funds are
    insufficient it will wait until there are enough funds or the account is closed.
    @param amount, the amount to withdraw
    @return true, if the withdrawal is successful
    @exception InterruptedException if another thread calls
    the interrupt method
     */

    public synchronized boolean withdraw(double amount) throws InterruptedException{
        while(amount > balance && isOpen()){
            System.out.println("Waiting for " + "some money....");
            wait();
        }

        boolean result  = false;

        if(isOpen()){
            balance -= amount;
            result = true;
        }

        return result;
    }

    public synchronized boolean isOpen(){
        return isOpen;
    }

    /* Method to deposit into the account provided that the account is open. When the deposit is successful it will notify all waiting threads of the deposit
        @param amount The amount to deposit
        @return true if the deposit is successful otherwise false*/

    public synchronized boolean deposit(double amount){
        if(isOpen()){
            balance += amount;
            notifyAll();
            return true;
        }
        else{
            return false;
        }
    }

    /*Close the bank account*/
    public synchronized void close(){
        isOpen = false;
        notifyAll();
    }


}
